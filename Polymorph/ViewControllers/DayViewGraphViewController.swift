//
//  DayViewGraphViewController.swift
//  PolyMorphWeather
//
//  Created by Darren Tuck on 2020/07/10.
//  Copyright © 2020 BitLab. All rights reserved.
//

import UIKit
import Charts

class DayViewGraphViewController: UIViewController {

    @IBOutlet var lineChartView: LineChartView!
    var data: [Hourly]? = nil
        
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
                
        refreshData()
    }
    
    func refreshData() {
        DispatchQueue.main.async {
            self.updateChart()
        }
    }

    func updateChart() {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "H:mm"

        
        var labels: [String] = []
        var dataEntriesTemp: [ChartDataEntry] = []
        var dataEntriesRain: [ChartDataEntry] = []
        var dataEntriesWind: [ChartDataEntry] = []

        for i in 0..<self.data!.count {
            let entry = self.data![i]
            
            let date = Date(timeIntervalSince1970: Double(entry.dt))
            let dt = dateFormatter.string(from: date)

            labels.append("\(dt)")
            
            let dataEntryTemp = ChartDataEntry(x: Double(i), y: entry.temp, data: entry)
            dataEntriesTemp.append(dataEntryTemp)

            let dataEntryRain = ChartDataEntry(x: Double(i), y: (entry.rain?.the1H ?? 0), data: entry)
            dataEntriesRain.append(dataEntryRain)

            let dataEntryWind = ChartDataEntry(x: Double(i), y: (entry.windSpeed ?? 0), data: entry)
            dataEntriesWind.append(dataEntryWind)
        }

        let chartDataSetTemp = LineChartDataSet(entries: dataEntriesTemp, label: "Temp")
        chartDataSetTemp.circleRadius = 3
        chartDataSetTemp.circleHoleRadius = 1
        chartDataSetTemp.drawValuesEnabled = false
        chartDataSetTemp.colors = [.red]
        chartDataSetTemp.mode = .cubicBezier
        chartDataSetTemp.circleColors = [UIColor.red.withAlphaComponent(0.1)]

        let chartDataSetWind = LineChartDataSet(entries: dataEntriesWind, label: "Wind")
        chartDataSetWind.circleRadius = 3
        chartDataSetWind.circleHoleRadius = 1
        chartDataSetWind.drawValuesEnabled = false
        chartDataSetWind.colors = [.orange]
        chartDataSetWind.mode = .cubicBezier
        chartDataSetWind.circleColors = [UIColor.orange.withAlphaComponent(0.1)]

        let chartDataSetRain = LineChartDataSet(entries: dataEntriesRain, label: "Rain")
        chartDataSetRain.circleRadius = 3
        chartDataSetRain.circleHoleRadius = 1
        chartDataSetRain.drawValuesEnabled = false
        chartDataSetRain.colors = [.blue]
        chartDataSetRain.mode = .cubicBezier
        chartDataSetRain.circleColors = [UIColor.blue.withAlphaComponent(0.1)]

        let chartData = LineChartData(dataSets: [chartDataSetTemp, chartDataSetWind, chartDataSetRain])

        lineChartView.data = chartData
        
        lineChartView.xAxis.valueFormatter = IndexAxisValueFormatter(values: labels)
        lineChartView.xAxis.labelPosition = .bottom
        lineChartView.xAxis.drawGridLinesEnabled = false
        lineChartView.xAxis.avoidFirstLastClippingEnabled = true

        lineChartView.rightAxis.drawAxisLineEnabled = false
        lineChartView.rightAxis.drawLabelsEnabled = false

        lineChartView.leftAxis.drawAxisLineEnabled = false
        lineChartView.pinchZoomEnabled = true
        lineChartView.doubleTapToZoomEnabled = true
        lineChartView.legend.enabled = true
    }
}

