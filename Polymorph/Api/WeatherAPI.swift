//
//  WeatherAPI.swift
//  PolyMorphWeather
//
//  Created by Darren Tuck on 2020/07/11.
//  Copyright © 2020 BitLab. All rights reserved.
//

import Foundation

class WeatherAPI {
    
    private static func get(endpoint: String, urlArgs: String, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) {

        let fullUrl = URL(string: "\(Constants.apiUrlv1)\(endpoint)?&units=metric&appid=\(Constants.apiKey)&\(urlArgs)")!

        let session = URLSession.shared
        var request = URLRequest(url: fullUrl)
        request.httpMethod = "GET"
        request.setValue(Constants.apiKey, forHTTPHeaderField: "Authorization")

        let task = session.dataTask(with: request, completionHandler: completionHandler)
        task.resume()
    }
    
    static public func getWeatherData(lat: Double, lon: Double, completion: @escaping (WeatherDataResponse?) -> Void) {
           
           WeatherAPI.get(endpoint: "onecall", urlArgs: "lat=\(lat)&lon=\(lon)", completionHandler: { data, response, error in
               let result: WeatherDataResponse = try! JSONDecoder().decode(WeatherDataResponse.self, from: data!)
               completion(result)
               }
           )
       }
    
}
