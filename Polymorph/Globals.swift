//
//  Globals.swift
//  PolyMorphWeather
//
//  Created by Darren Tuck on 2020/07/11.
//  Copyright © 2020 BitLab. All rights reserved.
//

struct Constants {

    static let apiUrlv1: String = "https://api.openweathermap.org/data/2.5/"
    
    static let apiKey: String = "a7d53af2586715adc829212bf06dd97c"

}

