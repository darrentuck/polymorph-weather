//
//  ImageLoader.swift
//  PolymorphWeather
//
//  Created by Darren Tuck on 2020/08/03.
//  Copyright © 2020 BitLab. All rights reserved.
//

import Foundation
import UIKit


class ImageLoader {
    
    var task: URLSessionDownloadTask!
    var session: URLSession!
    var cache: NSCache<NSString, UIImage>!
    
    init() {
        session = URLSession.shared
        task = URLSessionDownloadTask()
        self.cache = NSCache()
    }
    
     func obtainImageWithPath(imagePath: String, completionHandler: @escaping (UIImage) -> ()) {
           if let image = self.cache.object(forKey: imagePath as NSString) {
               DispatchQueue.main.async {
                   completionHandler(image)
               }
           } else {
               let url: URL! = URL(string: imagePath)
               task = session.downloadTask(with: url, completionHandler: { (location, response, error) in
                   if let data = try? Data(contentsOf: url) {
                       let img: UIImage! = UIImage(data: data)
                       self.cache.setObject(img, forKey: imagePath as NSString)
                       DispatchQueue.main.async {
                           completionHandler(img)
                       }
                   }
               })
               task.resume()
           }
       }
}
 
