//
//  ForecastGraphAndListViewController.swift
//  PolyMorphWeather
//
//  Created by Darren Tuck on 2020/07/10.
//  Copyright © 2020 BitLab. All rights reserved.
//

import UIKit
import Charts
import CoreLocation

class ForecastGraphAndListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, CLLocationManagerDelegate {

    @IBOutlet var tableView: UITableView!
    @IBOutlet var lineChartView: LineChartView!
    
    var lat: Double = 0.0
    var lon: Double = 0.0
    var selectedRow: Int = 0
    
    private var data: WeatherDataResponse? = nil
    
    lazy var locationManager: CLLocationManager = {
        var _locationManager = CLLocationManager()
        _locationManager.delegate = self
        _locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        _locationManager.activityType = . automotiveNavigation
        _locationManager.distanceFilter = 10.0

        return _locationManager
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
                
        refreshData()

        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
    }
    
    func refreshData() {
        WeatherAPI.getWeatherData(lat: self.lat, lon: self.lon, completion: { data in

            self.data = data
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
                self.updateChart()
            }
        })
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let dailyData = self.data?.daily {
            return dailyData.count
        }
        
        return 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "dailyCell") as! ForecastEntryTableViewCell
        cell.build(withData: data!.daily[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        self.selectedRow = indexPath.row
//        self.performSegue(withIdentifier: "dayViewSegue", sender: nil)    //TODO: no action for now, we use the bar button
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "dayViewSegue" {
            let vc = segue.destination as! DayViewGraphViewController
            vc.data = self.data?.hourly
        }
    }

    func updateChart() {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "d MMM"

        
        var labels: [String] = []
        var dataEntriesMax: [ChartDataEntry] = []
        var dataEntriesMin: [ChartDataEntry] = []

        for i in 0..<self.data!.daily.count {
            let entry = self.data!.daily[i]
            
            let date = Date(timeIntervalSince1970: Double(entry.dt))
            let dt = dateFormatter.string(from: date)

            labels.append("\(dt)")
            
            let dataEntryMin = ChartDataEntry(x: Double(i), y: round(entry.temp.min), data: entry)
            dataEntriesMin.append(dataEntryMin)

            let dataEntryMax = ChartDataEntry(x: Double(i), y: round(entry.temp.max), data: entry)
            dataEntriesMax.append(dataEntryMax)
        }

        let chartDataSetMax = LineChartDataSet(entries: dataEntriesMax, label: "Max")
        chartDataSetMax.circleRadius = 5
        chartDataSetMax.circleHoleRadius = 2
        chartDataSetMax.drawValuesEnabled = true
        chartDataSetMax.colors = [.red]
        chartDataSetMax.mode = .cubicBezier
        chartDataSetMax.circleColors = [UIColor.red.withAlphaComponent(0.1)]

        let chartDataSetMin = LineChartDataSet(entries: dataEntriesMin, label: "Min")
        chartDataSetMin.circleRadius = 5
        chartDataSetMin.circleHoleRadius = 2
        chartDataSetMin.drawValuesEnabled = true
        chartDataSetMin.colors = [.blue]
        chartDataSetMin.mode = .cubicBezier
        chartDataSetMin.circleColors = [UIColor.blue.withAlphaComponent(0.1)]

        
        let chartData = LineChartData(dataSets: [chartDataSetMax, chartDataSetMin])

        lineChartView.data = chartData
        
        lineChartView.xAxis.valueFormatter = IndexAxisValueFormatter(values: labels)
        lineChartView.xAxis.labelPosition = .bottom
        lineChartView.xAxis.drawGridLinesEnabled = false
        lineChartView.xAxis.avoidFirstLastClippingEnabled = true

        lineChartView.rightAxis.drawAxisLineEnabled = false
        lineChartView.rightAxis.drawLabelsEnabled = false

        lineChartView.leftAxis.drawAxisLineEnabled = false
        lineChartView.pinchZoomEnabled = true
        lineChartView.doubleTapToZoomEnabled = true
        lineChartView.legend.enabled = true
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location0 = locations[0]
        self.lat = location0.coordinate.latitude
        self.lon = location0.coordinate.longitude

        self.refreshData()
    }
    
}

