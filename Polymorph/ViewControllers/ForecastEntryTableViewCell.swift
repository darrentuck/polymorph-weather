//
//  ForecastEntryTableViewCell.swift
//  CovidShop
//
//  Created by Darren Tuck on 2020/05/03.
//  Copyright © 2020 BitLab. All rights reserved.
//

import UIKit

class ForecastEntryTableViewCell: UITableViewCell {

    @IBOutlet var rowImage: UIImageView!
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var descriptionLabel: UILabel!
    @IBOutlet var tempLabel: UILabel!
    @IBOutlet var subtitleLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        tempLabel.layer.masksToBounds = true
        tempLabel.layer.cornerRadius = 12
        tempLabel.textColor = .black
        tempLabel.backgroundColor = .init(red: 94/255, green: 198/255, blue: 255/255, alpha: 1)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    func build(withData: Daily) {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "d MMMM"

        let windspeed = Int(withData.windSpeed * 3.6)
        subtitleLabel.text = "Wind: \(windspeed) km/h"

        if let rain = withData.rain {
            subtitleLabel.text! += "  |  Rain: \(rain) ml"
        }
        else {
            subtitleLabel.text! += "  |  Rain: -"
        }

        let tempMax = Int(withData.temp.max)
        let tempMin = Int(withData.temp.min)
        tempLabel.text = "\(tempMax)°C / \(tempMin)°C "

        let date = Date(timeIntervalSince1970: Double(withData.dt))
        let dt = dateFormatter.string(from: date)
        dateLabel.text = dt

        descriptionLabel.text = "\(withData.weather[0].description)"
        
        ImageLoader().obtainImageWithPath(imagePath: "https://openweathermap.org/img/wn/\(withData.weather[0].icon)@2x.png") { (image) in
                self.rowImage.image = image
        }
    }

}
