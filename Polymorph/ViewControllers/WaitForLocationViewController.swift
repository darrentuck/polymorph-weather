//
//  WaitForLocationViewController.swift
//  PolymorphWeather
//
//  Created by Darren Tuck on 2020/08/03.
//  Copyright © 2020 BitLab. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation

class WaitForLocationViewController: UIViewController, CLLocationManagerDelegate {

    var lat: Double = 0.0
    var lon: Double = 0.0
    
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    
    lazy var locationManager: CLLocationManager = {
        var _locationManager = CLLocationManager()
        _locationManager.delegate = self
        _locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        _locationManager.activityType = . automotiveNavigation
        _locationManager.distanceFilter = 10.0

        return _locationManager
    }()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        
        activityIndicator.startAnimating()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "forecastSegue" {
            
            let nc = segue.destination as! UINavigationController
            let vc = nc.viewControllers[0] as! ForecastGraphAndListViewController
            vc.lat = self.lat
            vc.lon = self.lon
        }
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location0 = locations[0] 
        self.lat = location0.coordinate.latitude
        self.lon = location0.coordinate.longitude
        
        self.performSegue(withIdentifier: "forecastSegue", sender: nil)
    }
    
}
